<?php
header ("Content-Type: text/html; charset=utf-8");

include 'db.php';

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
    <!--[if lt IE 9]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'><![endif]-->

    <title>Добро пожаловать!</title>

    <link rel="stylesheet" href="style.css">

</head>
<body>
    <form action="go-form.php" id="form" method="post">
    <div class="wish-list">
        <div class="wish-list__wrapper">
            <div class="wish-list__title">СТРАНИЦА
                ЖЕЛАНИЙ
                ДЛЯ
                ИБРАГИМА
                И ЮЛИ
            </div>
            <div class="list">
                <?
                    $sql = 'select * from list';

                    $res = $mysqli->query($sql)->fetch_all();

                    //print_r($res);

                    $counter = 0;

                    foreach ($res as $item) {
                ?>
                    <div class="item"><label class="label <? if($item[2]) echo 'done' ?>" for="d<?=$item[0]?>"><input class="input" value="<?=$item[0]?>" data-text="<?=$item[1]?>"  type="checkbox" name="<?=$item[0]?>" id="d<?=$item[0]?>"> <?=$item[1]?></label></div>
                <?  $counter++;
                    }?>

                <div class="item" id="addOther"><label class="label label_add" for="d-other">Другое желание</label>
                            <div class="add-block ">
                                <input class="addOther" name="add" type="text">
                            </div>
                </div>
            </div>
            <input type="submit" class="btn" value="выбрать желание">
            <div class="discr">* Я, <strong>Корниенко Александр</strong>, обязуюсь добросовестно выполнить все указанные желания</div>
        </div>
    </div>
    </form>

    <script src="jquery.js"></script>
    <script src="scripts.js"></script>
</body>
</html>
