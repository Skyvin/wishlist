$(function () {

    $('.input:not(.addOther)').on('change', function () {

        $(this).closest('label:not(.done)').toggleClass('label-checked');
        console.log($(this).is(':checked'))

        $(this).closest('label.done').removeClass('done');

    })


    $('#form').submit(function (e) {
        e.preventDefault();

        var data = {
            wishes: [],
            newWish: ''
        }
        $('.input:checked').each(function (i, elem) {
            data.wishes.push({
                id: $(elem).val(),
                name: $(elem).data('text')
            })
        })

            data.newWish = $('.addOther').eq(0).val();


        $.post({
            url: 'go-form.php',
            data: data
        }, function (data) {
            $('.input:checked').each(function (i, elem) {
                $(elem).closest('label').toggleClass('done');
            })
            console.log(data);
            alert('Изменено!');
            location.reload();
        });
    })

    $('.label_add').click(function () {
        $('.add-block ').toggleClass('active').find('input').focus()
    })

});